
import React,{Component} from "react"
import {Text} from "react-native"
import { createBottomTabNavigator } from 'react-navigation-tabs';
import {createAppContainer} from "react-navigation"
import home from "./components/homepage"
import allMovie from "./components/allmovie"
import allActor from "./components/allactor"
import profileheadermager from "./profileheadermager"
import Icon from "react-native-vector-icons/FontAwesome";
import topHeader from "./TopHeader";
import Actor from "./actor"
import Movie from "./movie"


const pageManager = createBottomTabNavigator(
  {
    Home: { screen: topHeader ,
      navigationOptions: () => ({
        tabBarIcon: ({tintColor}) => (
            <Icon
                name="home"
                color={tintColor}
                size={24}
            />
        )
    }),
    },
    allMovie:{screen:Movie, navigationOptions: () => ({
      tabBarIcon: ({tintColor}) => (
          <Icon
              name="clone"
              color={tintColor}
              size={24}
          />
      ),
      tabBarLabel : ({tintColor}) => (
        <Text style  ={{color:tintColor,textAlign:"center"}}>All Categorie</Text>
      )
  }
  ) },
    allActor:{screen:Actor, navigationOptions: () => ({
      tabBarIcon: ({tintColor}) => (
          <Icon
              name="user"
              color={tintColor}
              size={24}
          />
      ),
      tabBarLabel : ({tintColor}) => (
        <Text style  ={{color:tintColor,textAlign:"center"}}>All Actor</Text>
      )
  })},
    Profile:{screen: profileheadermager , navigationOptions: () => ({
      tabBarIcon: ({tintColor}) => (
          <Icon
              name="ellipsis-h"
              color={tintColor}
              size={24}
          />
      )
  })},


 
  },
  {
    initialRouteName: 'Home',
    tabBarOptions: {
      activeTintColor: "#fff",
      labelStyle: {
        fontSize: 12,
      },
      style: {
        borderTopColor:"#000",
        backgroundColor: 'black',
      },
    }
  }
);
const AppContainer = createAppContainer(pageManager) 
export default AppContainer;