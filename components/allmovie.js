import React, { Component } from 'react';
import { StyleSheet, View, Text, Image,ScrollView,TouchableOpacity, TabBarIOS} from 'react-native';
import Search from 'react-native-search-box';
import Category from './Category'
import { Tile } from 'react-native-elements';
export default class allMovie extends Component {
    state = {
        search: '',
      };
      updateSearch = search => {
        this.setState({ search });
      };
      static navigationOptions = {
          headerShown:false
      };
  render() {
    const { search } = this.state;
    const {navigate } =this.props.navigation
    return (
     
     <View style={styles.container}>
    <TouchableOpacity onPress={() =>navigate('RE')}>
    <Search
    backgroundColor="#000">   
    </Search>
    </TouchableOpacity>
    <ScrollView>
  <View style={{ height: 200, marginTop: 0.5 ,padding:10 }}>
        <Text style={{color:"white" , textAlign:"center", fontWeight:"bold"}} onPress={() =>navigate('allMovie')}>Love Movie</Text>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                <TouchableOpacity onPress={() =>navigate('movieInfor')}>
                                <Category imageUri={require('../assets/(6)(6).jpg')  }
                                    name="movie1" 
                                /></TouchableOpacity>
                                <Category imageUri={require('../assets/(7)(7).jpg')}
                                    name="movie3"
                                />
                                <Category imageUri={require('../assets/(8)(8).jpg')}
                                    name="movie4"
                                />
                                <Category imageUri={require('../assets/(9)(9).jpg')}
                                    name="movie5"
                                />
                                <Category imageUri={require('../assets/(10)(10).jpg')}
                                    name="movie6"
                                />
                            </ScrollView>                            
                        </View>
                        <View style={{ height: 200, marginTop: 0.5 ,padding:10 }}>
        <Text style={{color:"white" , textAlign:"center", fontWeight:"bold"}}>Action Movie</Text>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                <Category imageUri={require('../assets/(5).jpg')}
                                    name="Home"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Home"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                            </ScrollView>
                        </View>
                        <View style={{ height: 200, marginTop: 0.5 ,padding:10 }}>
        <Text style={{color:"white" , textAlign:"center" ,fontWeight:"bold"}}>Horro Actors</Text>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                <Category imageUri={require('../assets/(5).jpg')}
                                    name="Home"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Home"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                            </ScrollView>
                        </View>
                        <View style={{ height: 200, marginTop:0.5,padding:10  }}>
        <Text style={{color:"white" , textAlign:"center", fontWeight:"bold"}}>Cartoon</Text>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                <Category imageUri={require('../assets/(5).jpg')}
                                    name="Home"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Home"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                            </ScrollView>
                        </View></ScrollView></View>
    );
  }
}

const styles = StyleSheet.create({
    container :{
        flex:1,
        paddingTop:30,
        backgroundColor:"black"
    },
  gridView: {
    marginTop: 20,
    flex: 1,
    backgroundColor:"black"},
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    textAlign:"center",
    color: '#ffffff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});


