import React, { Component } from 'react';
import { StyleSheet, View, Text, Image,ScrollView,TouchableOpacity} from 'react-native';
import { FlatGrid } from 'react-native-super-grid';
import Header from "./movieInfor";
import Search from 'react-native-search-box';
import Category from './Category'
import {creacteStackNavigator} from "react-navigation-stack"
import {createAppNavigator} from "react-navigation"

export default class allActor extends Component {
    state = {
        search: '',
      };
      updateSearch = search => {
        this.setState({ search });
      };
      static navigationOptions = {
        headerShown:false
        
      };
  render() {
    const { search } = this.state;
    const { navigate }= this.props.navigation
    return (
     
     <View style={styles.container}>
         <TouchableOpacity onPress={() =>navigate('RE')}>
    <Search 
      backgroundColor="black"
    
    ></Search>
    </TouchableOpacity>
    <ScrollView>
  <View style={{ height: 200, marginTop: 0.5 ,padding:10 }}>
        <Text style={{color:"white" , textAlign:"center", fontWeight:"bold"}} onPress={() =>navigate('allMovie')}>Austria Actor</Text>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                 <TouchableOpacity onPress={() =>navigate('actorInfor')}>
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Home"
                                />
                                </TouchableOpacity>
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="movie2"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="movie3"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="movie4"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="movie5"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="movie6"
                                />
                            </ScrollView>                            
                        </View>
                        <View style={{ height: 200, marginTop: 0.5 ,padding:10 }}>
        <Text style={{color:"white" , textAlign:"center", fontWeight:"bold"}}>America Actor</Text>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Home"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Home"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                            </ScrollView>
                        </View>
                        <View style={{ height: 200, marginTop: 0.5 ,padding:10 }}>
        <Text style={{color:"white" , textAlign:"center" ,fontWeight:"bold"}}>France Actor</Text>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Home"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Home"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                            </ScrollView>
                        </View>
                        <View style={{ height: 200, marginTop:0.5,padding:10  }}>
        <Text style={{color:"white" , textAlign:"center", fontWeight:"bold"}}>Asia Actor</Text>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Home"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Home"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                            </ScrollView>
                        </View></ScrollView></View>
    );
  }
}

const styles = StyleSheet.create({
    container :{
        flex:1,
        backgroundColor:"black",
        paddingTop:20
    },
  gridView: {
    marginTop: 20,
    flex: 1,
    backgroundColor:"black"},
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    textAlign:"center",
    color: '#ffffff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});


