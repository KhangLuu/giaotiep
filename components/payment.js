import React, { Component } from 'react';
import { Text, View ,StyleSheet,FlatList} from 'react-native';

import Header from "./movieInfor";
const DATA =[
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    name: "VietComBank",
    decrible:"09********1",
    color:"#07e81a"

  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    name: "Sacombank",
    decrible:"09********1",
    color:"#990aff"

  }
,
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    name: "TP Bank",
    decrible:"09********1",
    color:"#0d6adb"
  },
 
];
function Item({ name,decrible,color }) {
  return (
    <View style={{padding:10}}>
    <View style={styles.main}>
      <View  style={{flexDirection:"row"}}>
      <Text style={{  fontSize: 17, fontWeight:"bold",color:color,paddingBottom:10}}>{name}</Text>
      <Text style={styles.des}>{decrible}</Text>
      </View>
    </View>
    </View>
  );
}

export default class payment extends Component {
  static navigationOptions = {
    title: 'Payment Card Manager',
    //Sets Header text of Status Bar
    headerStyle: {
      backgroundColor: '#000',
      //Sets Header color
    },
    headerTintColor: '#fff',
    //Sets Header text color
    headerTitleStyle: {
      fontWeight: 'bold',
      flex:1,
      textAlign:'left'

      //Sets Header text style
    },
  };
  render() {
    return (
      <View style={styles.container}>
         <View>
         <FlatList
        data={DATA}
        renderItem={({ item }) => <Item name={item.name} decrible ={item.decrible} color= {item.color}/>}
      />
         </View>
         </View>
        
     
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:"black"
  },
  name: {
    fontSize: 17,
    fontWeight:"bold",
    color: "white",
    paddingBottom:10,
  },
  main:{
    borderRadius: 6,
    borderWidth: 0.5,
    borderColor: '#5c5c5c',
    backgroundColor:'#5c5c5c',    
  },
  des:{
    fontSize: 15,
    textAlign: 'right',
    color: 'white',
    flex:1,
    fontWeight:"bold"
  }
});
