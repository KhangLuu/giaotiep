import React, { Component } from 'react';
import { StyleSheet, View, Text,ScrollView, Button , Image, TouchableOpacity} from 'react-native';
import { SectionGrid } from 'react-native-super-grid';
import Slideshow from 'react-native-image-slider-show';
import Category from "./Category"
import Icon from "react-native-vector-icons/FontAwesome"
import test from"./test"
class LogoTitle extends React.Component {
    render() {
      return (
          <View style={{flexDirection:"row"}}>
        <Image
          source={require('../assets/(2).jpg')}
          style={{ width: 20, height: 20 }}

        />
        <Text style={{  fontWeight: 'bold',
          flex:1,
          size:20,
          textAlign:'left',
          fontStyle:"italic",
          color:"#e8e8e8"
    }}>VIET PHIM ONLINE</Text></View>
      );
    }
  }

export default class home extends Component {
    
    static navigationOptions =({navigation}) => {

        return{ 
        headerTitle: () => <LogoTitle />,
        headerStyle: {
          backgroundColor: '#000',
          //Sets Header color
          height:50
        },
        headerRight: 
            <View  style={{flexDirection:"row"}}>
              <Icon name="search" color="#fff" size={20} style={{paddingHorizontal:20}} onPress={() => this.props.navigation.navigate("noti")} />
              <Icon name="bell" color="#fff"    size={20} style= {{paddingRight:5}} onPress={() => navigate ('noti')}/> 
            </View>
          
      }};

  render() {
    const { navigate } = this.props.navigation;
    return (
        <ScrollView style={styles.container}>
            <Slideshow 
            onPress={()=>navigate("movieInfor")}
            scrollEnabled = {true}
            titleStyle={{color:"#e8e8e8", fontSize:30, fontWeight:"bold",  textShadowColor:'#585858',
            textShadowOffset:{width: 5, height: 5},
            textShadowRadius:10,}}
            containerStyle={{borderColor:"red"}}
            
      dataSource={[
        {   title: 'Koe no Katachi',
            caption: 'Movie1',
            url:(require("../assets/(13)(13).jpg")) },
        { 
            title: 'Koe no Katachi',
            caption: 'Movie2',
            url:(require("../assets/(3)(3).jpg"))},
        {  title: 'Tamako love storu',
        caption: 'Movie3',
        url:(require("../assets/(15)(15).jpg")) },
        {  title: 'Tamako love story',
        caption: 'Movie4',
        url:(require("../assets/(16)(16).jpg")) },
    
    ]}/>
    <View style={{ height: 200, marginTop: 0.5 ,padding:10 }}>
        <Text style={{color:"white" , textAlign:"left", fontWeight:"bold" ,paddingBottom:5}} onPress={() =>navigate('allMovie')}>New Movie</Text>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            > 
                                <TouchableOpacity onPress={() =>navigate('movieInfor')}>
                                <Category imageUri={require('../assets/(6)(6).jpg')  }
                                    name="movie1" 
                                /></TouchableOpacity>
                                <TouchableOpacity onPress={() =>navigate('movieInfor')}>
                                <Category imageUri={require('../assets/(7)(7).jpg')}
                                    name="movie3"
                                /></TouchableOpacity>
                                <TouchableOpacity onPress={() =>navigate('movieInfor')}>
                                <Category imageUri={require('../assets/(8)(8).jpg')}
                                    name="movie4"
                                /></TouchableOpacity>
                                <Category imageUri={require('../assets/(9)(9).jpg')}
                                    name="movie5"
                                />
                                <Category imageUri={require('../assets/(10)(10).jpg')}
                                    name="movie6"
                                />
                                <TouchableOpacity onPress={() =>navigate('movieInfor')}>
                                <Category imageUri={require('../assets/(5).jpg')}
                                    name="movie2"
                                /></TouchableOpacity>
                            </ScrollView>                            
                        </View>
                        <View style={{ height: 200, marginTop: 0.5 ,padding:10 }}>
        <Text style={{color:"white" , textAlign:"left", fontWeight:"bold" ,paddingBottom:5}} onPress={() =>navigate('noti')}>Trending Now Movies</Text>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                <Category imageUri={require('../assets/(5).jpg')}
                                    name="Home"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Home"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                            </ScrollView>
                        </View>
                        <View style={{ height: 200, marginTop: 0.5 ,padding:10 }}>
        <Text style={{color:"white" , textAlign:"left" ,fontWeight:"bold", paddingBottom:5}} onPress={() =>navigate('allActor')}>Hot Actors</Text>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                <TouchableOpacity onPress={() =>navigate('actorInfor')}>
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Home"
                                />
                                </TouchableOpacity>
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Home"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                            </ScrollView>
                        </View>
                        <View style={{ height: 200, marginTop:0.5,padding:10  }}>
        <Text style={{color:"white" , textAlign:"left", fontWeight:"bold", paddingBottom:5}}>Movies Recommend For You</Text>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                <Category imageUri={require('../assets/(7)(7).jpg')}
                                    name="Home"
                                />
                                <Category imageUri={require('../assets/(6)(6).jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Home"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Experiences"
                                />
                                <Category imageUri={require('../assets/1.jpg')}
                                    name="Resturant"
                                />
                            </ScrollView>
                        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
container :{
    flex:1,
    backgroundColor:"black",
},
  gridView: {
    marginTop: 20,
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  sectionHeader: {
    flex: 1,
    fontSize: 15,
    fontWeight: '600',
    alignItems: 'center',
    textAlign:"center",
    backgroundColor: '#636e72',
    color: 'white',
    padding: 10,
    backgroundColor:"black"
  },
});