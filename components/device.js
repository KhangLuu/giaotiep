import React,{Component} from "react"
import {View, Text , Button, StyleSheet,FlatList} from "react-native"
import  Icon  from "react-native-vector-icons/FontAwesome";
export default class device extends Component {

  static navigationOptions = {
    title: 'Device Manager',
    //Sets Header text of Status Bar
    headerStyle: {
      backgroundColor: '#000',
      //Sets Header color
    },
    headerTintColor: '#fff',
    //Sets Header text color
    headerTitleStyle: {
      fontWeight: 'bold',
      flex:1,
      textAlign:'left'

      //Sets Header text style
    },
  };
  


    render(){
        return(
          <View style={styles.container}>
            <View style={{paddingBottom:20, paddingLeft:20}}>
            <View style={{flexDirection:"row", }}>
              <Icon name="desktop" style={{color:"yellow"}} size={20}></Icon>
              <Text style={{color:"red", fontWeight:"bold", paddingLeft:30}}>WEB</Text>
              <Icon name="square-o" style={{color:"white", flex:1,paddingLeft:200}} size={20}></Icon>
            </View>
            <Text style={{color:"white",paddingLeft:50}}>10/11/2019 18:04:32</Text>
            </View>
            <View style={{paddingBottom:20, paddingLeft:30}}>
            <View style={{flexDirection:"row", }}>
              <Icon name="mobile" style={{color:"yellow"}} size={20}></Icon>
              <Text style={{color:"green",fontWeight:"bold" ,paddingLeft:30}}>Mobile</Text>
              <Icon name="square-o" style={{color:"white",flex:1,paddingLeft:190}} size={20}></Icon>
            </View>
            <Text style={{color:"white",paddingLeft:40}}>10/11/2019 18:04:32</Text>
            </View>
            <View style={{flex : 1 , justifyContent:"center",alignItems:"center"}}><Button  title="Remove" color="#dbb13b" ></Button></View>
          </View>
        )
    }
 }
const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:"black",
    paddingTop:30 
  }
})