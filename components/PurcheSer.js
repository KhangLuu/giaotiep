import React,{Component} from "react";
import {FlatList, Text, View,StyleSheet} from "react-native";

const DATA =[
    {
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
      title: 'Basic package-1 day ',
      time:"Expired"
  
    },
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'Basic package-1 day',
        time:"29 days left "
    
      },
      {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'HBO Go packet 30 day',
        time:"Expired"
    
      },
    ]
    function Item({ title, time, index }) {
        return (

          <View style={{backgroundColor : index %2 == 0 ?"#404040":"black " }}>
            <Text style={styles.title}>{title}</Text>
            <Text style={{color:"#fff"}}>{time}</Text>
            </View>
)
          }
          
          export default class purcheSer extends Component {
            static navigationOptions = {
              title: 'Purchased service',
              //Sets Header text of Status Bar
              headerStyle: {
                backgroundColor: '#000',
                //Sets Header color
              },
              headerTintColor: '#fff',
              //Sets Header text color
              headerTitleStyle: {
                fontWeight: 'bold',
                flex:1,
                textAlign:'left'
          
                //Sets Header text style
              },
            };
            render() {
              return (
                <View style={styles.container}>
                   <View style={{paddingTop :50}}>
                   <FlatList
                  data={DATA}
                  renderItem={({ item, index }) => <Item title={item.title} name={item.name} decrible ={item.decrible} time={item.time} index={index} />}
                />
                   </View>
                   </View>
                  
               
              );
            }
          }
          const styles = StyleSheet.create({
            container: {
              flex: 1,
              backgroundColor:"black"
            },
            item: {
              backgroundColor: "black",
              paddingTop:5,
              paddingBottom:5,
              marginVertical:4,
              marginHorizontal: 16,
            },
            title: {
              fontSize: 20,
              fontWeight:"bold",
              color: "white",
              paddingBottom:10,
            },
            main:{
              borderRadius: 5,
              borderWidth: 0.5,
              borderColor: '#5c5c5c',
              backgroundColor:'#5c5c5c',    
            },
            name:{
              fontSize:15,
              color:"#ff8400",
              paddingLeft:5
            },
            des:{
              color:"white",
              shadowColor:"black",
              fontSize:10,
              paddingBottom:10,
              paddingLeft:5
          
              
            }
          });
