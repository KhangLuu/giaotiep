import React, { Component } from 'react';
import { StyleSheet, View, Text, Image} from 'react-native';
import { FlatGrid } from 'react-native-super-grid';
import Search from 'react-native-search-box';


export default class Movie extends Component {
    state = {
        search: '',
      };
      updateSearch = search => {
        this.setState({ search });
      };
      static navigationOptions = {
        title: 'Favorite Movie',
        //Sets Header text of Status Bar
        headerStyle: {
          backgroundColor: '#000',
          //Sets Header color
        },
        headerTintColor: '#fff',
        //Sets Header text color
        headerTitleStyle: {
          fontWeight: 'bold',
          flex:1,
          textAlign:'left'
    
          //Sets Header text style
        },
      };
  render() {
    const { search } = this.state;
    const items = [
      { name: 'Doctor Strange', code: '#1abc9c' }, { name: 'Doctor Strange', code: '#2ecc71' },
      { name: 'Doctor Strange', code: '#3498db' }, { name: 'Doctor Strange', code: '#9b59b6' },
      { name: 'Doctor Strange', code: '#34495e' }, { name: 'Doctor Strange', code: '#16a085' },
      { name: 'Doctor Strange', code: '#27ae60' }, { name: 'Doctor Strange', code: '#2980b9' },
      { name: 'Doctor Strange', code: '#8e44ad' }, { name: 'Doctor Strange', code: '#2c3e50' },
      { name: 'Doctor Strange', code: '#f1c40f' }, { name: 'Doctor Strange', code: '#e67e22' },
      
    ];

    return (
     
     <View style={styles.container}>
    <Search 
      backgroundColor="black"
    
    ></Search>
      <FlatGrid
        itemDimension={130}
        items={items}
        style={styles.gridView}                                                                                                                                                                                                                         
        // staticDimension={300}
        // fixed
        // spacing={20}
        renderItem={({ item, index }) => (
           
            <View style={[styles.itemContainer, { backgroundColor: "black" }]}>
            <Image source={require("../assets/1.jpg")} style={{ flex: 1,width: null,height: null, resizeMode: 'contain'}}></Image>
            <Text style={styles.itemName}>{item.name}</Text>
          </View>
          
          
        )}
      /></View>
    );
  }
}

const styles = StyleSheet.create({
    container :{
        flex:1,
        backgroundColor:"black"
    },
  gridView: {
    marginTop: 20,
    flex: 1,
    backgroundColor:"black"},
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    textAlign:"center",
    color: '#ffffff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});


