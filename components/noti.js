import React, { Component } from 'react';
import { Text, View ,StyleSheet,FlatList} from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";

const DATA =[
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'Jannuary,2020',
    name: "What is the identity of DoctorStrange",
    decrible:"the last trailer will reavale the truth of who is ther real DoctorStang",
    time:"10.00"

  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'Jannuary,2020',
    name: "What is the identity of DoctorStrange",
    decrible:"the last trailer will reavale the truth of who is ther real DoctorStang",
    time:"10.00"

  }
,
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'Jannuary,2020',
    name: "What is the identity of DoctorStrange",
    decrible:"the last trailer will reavale the truth of who is ther real DoctorStang",
    time:"10.00"
  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'Jannuary,2020',
    name: "What is the identity of DoctorStrange",
    decrible:"the last trailer will reavale the truth of who is ther real DoctorStang",
    time:"10.00"

  }
,
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28b2',
    title: 'Jannuary,2020',
    name: "What is the identity of DoctorStrange",
    decrible:"the last trailer will reavale the truth of who is ther real DoctorStang",
    time:"10.00"

  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28b3',
    title: 'Jannuary,2020',
    name: "What is the identity of DoctorStrange",
    decrible:"the last trailer will reavale the truth of who is ther real DoctorStang",
    time:"10.00"



  }
,
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28b1',
    title: 'Jannuary,2020',
    name: "What is the identity of DoctorStrange",
    decrible:"the last trailer will reavale the truth of who is ther real DoctorStang",
    time:"10.00"


  },
];
function Item({ title,name,decrible, time }) {
  return (
    <View style={styles.item}>
      <Text style={styles.title}>{title}</Text>
      <View style ={styles.main}>
      <View  style={{flexDirection:"row"}}>
      <Icon name ="circle" size={7} color="#ff8400" style={{paddingTop:8}}></Icon>
      <Text style={styles.name}>{name}</Text>
      <Text style={{paddingLeft :5 , fontSize:10 ,paddingTop:5,paddingRight:10}}>{time}</Text>
      </View>
      <Text style={styles.des}>{decrible}</Text>
      </View>

    </View>
  );
}

export default class noti extends Component {
  static navigationOptions = {
    title: 'Notification',
    //Sets Header text of Status Bar
    headerStyle: {
      backgroundColor: '#000',
      //Sets Header color
    },
    headerTintColor: '#fff',
    //Sets Header text color
    headerTitleStyle: {
      fontWeight: 'bold',
      flex:1,
      textAlign:'left'

      //Sets Header text style
    },
  };
  render() {
    return (
      <View style={styles.container}>
         <View>
         <FlatList
        data={DATA}
        renderItem={({ item }) => <Item title={item.title} name={item.name} decrible ={item.decrible} time={item.time} />}
      />
         </View>
         </View>
        
     
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:"black"
  },
  item: {
    backgroundColor: 'black',
    padding: 20,
    paddingTop:5,
    paddingBottom:5,
    marginVertical:4,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 20,
    fontWeight:"bold",
    color: "white",
    paddingBottom:10,
  },
  main:{
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: '#5c5c5c',
    backgroundColor:'#5c5c5c',    
  },
  name:{
    fontSize:15,
    color:"#ff8400",
    paddingLeft:5
  },
  des:{
    color:"white",
    shadowColor:"black",
    fontSize:10,
    paddingBottom:10,
    paddingLeft:5

    
  }
});
