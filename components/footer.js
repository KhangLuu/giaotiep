import React from 'react';
import { Text, View } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome"

//make a Component
const Footer = () => {
  const { footerStyle, bgFooter , textstyle, iconStyle } = styles;
  return (
    <View>
    <View style = {bgFooter}>
      <Icon name="home" size={40} color="white" style={iconStyle}/>    
      <Icon name="clone" size={40} color="white"style={iconStyle} />
      <Icon name="user" size={40} color="white" style={iconStyle}/>
      <Icon name="ellipsis-h" size={40} color="white" style={{paddingLeft:25,paddingRight:30}}/>
       </View>
       <View style={bgFooter}>
         <Text style={textstyle} >home</Text>
         <Text style={textstyle}>All categories</Text>
         <Text style={textstyle}>All actor</Text>
         <Text style={textstyle}> profile</Text>
       </View>
       </View>
  );
};

const styles = {
  bgFooter: {
    backgroundColor: 'black',
    justifyContent:'center',
    alignItems:'center',
    flexDirection: 'row'
    
  },
  footerStyle: {
    fontSize: 25,
    textAlign: 'center',
    margin: 10,
    color: 'white',
  },
  textstyle :{
    color:"white",
    paddingRight:14,
    paddingLeft:14,
  },
  iconStyle :{
    color:"white",
    paddingRight:26,
    paddingLeft:29,
    paddingTop:30
  }
};

module.exports = Footer;