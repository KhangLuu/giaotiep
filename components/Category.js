import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image,
    TouchableO
} from "react-native";

class Category extends Component {
    render() {
        return (
            <View style={{ height: 130, width: 130, marginLeft:5, borderWidth: 0.5, borderColor: 'gray' }}>
                <View style={{ flex: 2 }}>
                    <Image source={this.props.imageUri} 
                        style={{ flex: 1, width: null, height: null, resizeMode: 'cover' }}
                    />
                </View>
            </View>
        );
    }
}
export default Category;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor:"black"
    }
});