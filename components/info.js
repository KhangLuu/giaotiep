import React,{Component} from "react"
import {View, Text , Button, StyleSheet,FlatList} from "react-native"
import  Icon  from "react-native-vector-icons/FontAwesome";
export default class device extends Component {

  static navigationOptions = {
    title: 'Device Manager',
    //Sets Header text of Status Bar
    headerStyle: {
      backgroundColor: '#000',
      //Sets Header color
    },
    headerTintColor: '#fff',
    //Sets Header text color
    headerTitleStyle: {
      fontWeight: 'bold',
      flex:1,
      textAlign:'left'

      //Sets Header text style
    },
  };
  


    render(){
        return(
          <View style={styles.container}>
            <View style={{flexDirection:"row"}}>
              <Icon name="user" style={{color:"yellow" ,paddingRight:10,paddingLeft:20 , paddingBottom :30}} size={30} ></Icon>
              <Text style={{color:"yellow", fontWeight:"bold" ,paddingRight:20 ,fontSize:20}}>Username :</Text>
              <Text style={{color:"white", fontWeight:"bold", fontSize:20}}>Hoa Bui</Text>
              </View>
            <View style={{flexDirection:"row",}}>
              <Icon name="user" style={{color:"yellow",paddingRight:10,paddingLeft:20 , paddingBottom :30}} size={30}></Icon>
              <Text style={{color:"yellow",fontWeight:"bold",paddingRight:20 ,fontSize:20 }}>Fullname :</Text>
              <Text style={{color:"white", fontWeight:"bold",fontSize:20}}>Bui Gia Hoa </Text>
              </View>
              <View style={{flexDirection:"row"}}>
              <Icon name="envelope" style={{color:"yellow",paddingRight:10,paddingLeft:20 , paddingBottom :30}} size={30}></Icon>
              <Text style={{color:"yellow",fontWeight:"bold" ,paddingRight:20 ,fontSize:20}}>Email:</Text>
              <Text style={{color:"white", fontWeight:"bold",fontSize:20}}>aaaaaaaa@gmail.com</Text>
              </View>
              <View style={{flexDirection:"row"}}>
              <Icon name="unlock-alt" style={{color:"yellow",paddingRight:10,paddingLeft:20 , paddingBottom :30}} size={30}></Icon>
              <Text style={{color:"yellow",fontWeight:"bold" ,paddingRight:20 ,fontSize:20}}>Pass:</Text>
              <Text style={{color:"white",fontWeight:"bold",fontSize:20}}>*********</Text>
              <Icon name="pencil" style={{color:"white"}} size={20}></Icon>
              </View>
              <View style={{flex : 1 , justifyContent:"center",alignItems:"center"}}><Button  title="Update" color="#dbb13b" ></Button></View>
           
          </View>
        )
    }
 }
const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:"black",
    paddingTop:40
  }
})