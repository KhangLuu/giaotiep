import React, { Component } from 'react';
import {
  Animated,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  View,
  RefreshControl,
  ScrollView,
  TouchableOpacity

} from 'react-native';
import  Icon  from 'react-native-vector-icons/FontAwesome';
import Category from "./Category"
import { Tile } from 'react-native-elements';

const HEADER_MAX_HEIGHT = 300;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 73;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const title = "Arumo Ray"
const des =" Amuro Ray (アムロ・レイ Amuro Rei?) is one of the main characters from the Universal Century timeline. He is the protagonist of Mobile Suit Gundam, a supporting character in the sequel Mobile Suit Zeta Gundam, and the protagonist of Mobile Suit Gundam: Char's Counterattack. A Gundam pilot and a Newtype, he is most famous for piloting the powerful RX-78-2 Gundam during the One Year War. "


export default class actorInfor extends Component {
  static navigationOptions = {
    title:"",
    headerTransparent: {
      position: 'absolute',
      backgroundColor: 'transparent',
      zIndex: 100,
      top: 0,
      left: 0,
      right: 0
}
   }
  constructor(props) {
    super(props);

    this.state = {
      scrollY: new Animated.Value(
        // iOS has negative initial scroll value because content inset...
        Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
      ),
      refreshing: false,
    };
  }

  _renderScrollViewContent() {
    return (
      <View style={styles.scrollViewContent}>
  <Text style ={{color:"white", fontSize:30, paddingLeft:10, fontStyle:"italic", fontWeight:"bold", borderBottomColor:"white" ,borderBottomWidth:0.5,paddingBottom:3}}>{title}</Text>
          <View style={{flexDirection:"row",}}>
            <Text style ={{color:"white", paddingHorizontal:10 ,fontSize:20}}>AGE</ Text>
            <Text style ={{color:"white"  ,paddingHorizontal:5 ,fontSize:20}}>30</Text>
            </View>
            <View style={{ padding:10}}>
            <Text style ={{color:"white", fontSize:20} }>Describle :</Text>
            <View style={{ borderRadius: 4,borderWidth: 0.5,borderColor: '#d6d7da',}}>
            <Text style ={{color:"white",margin:5}}>{des}</Text>
            </View>
            </View>
            <View style={{ height: 200, marginTop: 0.5 ,padding:10 }}>
            <Text style={{color:"white" , textAlign:"center",backgroundColor:"#1f1f1f", fontSize:20,paddingBottom:10}}>Actor's Movie</Text>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                <Category imageUri={require('../assets/(18)(18).jpg')}
                                    name="movie2"
                                />
                                <Category imageUri={require('../assets/(19)(19).jpg')}
                                    name="movie3"
                                />
                                <Category imageUri={require('../assets/(20)(20).jpg')}
                                    name="movie4"
                                />
                                <Category imageUri={require('../assets/(18)(18).jpg')}
                                    name="movie5"
                                />
                                <Category imageUri={require('../assets/(19)(19).jpg')}
                                    name="movie6"
                                />
                            </ScrollView>                            
                        </View>

        </View>
    );
  }

  render() {
    // Because of content inset the scroll value will be negative on iOS so bring
    // it back to 0.
    const scrollY = Animated.add(
      this.state.scrollY,
      Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
    );
    const headerTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -HEADER_SCROLL_DISTANCE],
      extrapolate: 'clamp',
    });

    const imageOpacity = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0],
      extrapolate: 'clamp',
    });
    const imageTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 100],
      extrapolate: 'clamp',
    });

    const titleScale = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0.8],
      extrapolate: 'clamp',
    });
    const titleTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 0, -8],
      extrapolate: 'clamp',
    });

    return (
      <View style={styles.fill}>
        <StatusBar
          translucent
          barStyle="light-content"
          backgroundColor="rgba(0, 0, 0, 0.251)"
        />
        <Animated.ScrollView
          style={styles.fill}
          scrollEventThrottle={1}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
            { useNativeDriver: true },
          )}
        >
          {this._renderScrollViewContent()}
        </Animated.ScrollView>
        <Animated.View
          pointerEvents="none"
          style={[
            styles.header,
            { transform: [{ translateY: headerTranslate }] },
          ]}
        >
          <Animated.Image
            style={[
              styles.backgroundImage,
              {
                opacity: imageOpacity,
                transform: [{ translateY: imageTranslate }],
              },
            ]}
            source={require('../assets/(17)(17).jpg')}
          />
        </Animated.View>
        <Animated.View
          style={[
            styles.bar,
            {
              transform: [
                { scale: titleScale },
                { translateY: titleTranslate },
              ],
            },
          ]}
        >
  <Text style={styles.title}>{title}</Text>
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  fill: {
    flex: 1,
    backgroundColor:"#000"
  },
  content: {
    flex: 1,
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#000',
    overflow: 'hidden',
    height: HEADER_MAX_HEIGHT,
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },
  bar: {
    backgroundColor: 'transparent',
    marginTop: Platform.OS === 'ios' ? 28 : 38,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  title: {
    color: 'white',
    fontSize: 20,
    fontStyle:"italic",
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
     textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10
  },
  scrollViewContent: {
   
    paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
  },
  row: {
    height: 40,
    margin: 16,
    backgroundColor: '#D3D3D3',
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon :{
    color:"#fff",
    paddingHorizontal:10
  }
});