import React, { Component } from 'react';
import { Text, View ,StyleSheet} from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome"
export default class profile extends Component {
  static navigationOptions = {
    title: '',
    //Sets Header text of Status Bar
    headerStyle: {
      backgroundColor: '#000',
      //Sets Header color
    },
    headerTintColor: '#fff',
    //Sets Header text color
    headerTitleStyle: {
      fontWeight: 'bold',
      //Sets Header text style
    },
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={styles.textlayout}>
        <Text style={styles.textHead}> Hi!</Text>
        <Text style={{color:"#c26e0e" ,fontWeight:"bold", paddingBottom:40 ,fontSize:25}} onPress={() =>navigate('SecondPage')} >Hoa Bui</Text>
        <Text style={styles.textEdit} onPress={() =>navigate('ThreePage')}> Personal information</Text>
        <Text style={styles.textEdit} onPress={() =>navigate('FourPage')}> Device Manager</Text>
        <Text style={styles.textEdit} onPress={() =>navigate('FivePage')}> Payment cart Manager</Text>
        <Text style={styles.textEdit} onPress={() =>navigate('SixPage')}> TransAction History</Text>
        <Text style={styles.textEdit} onPress={() =>navigate('SevenPage')}> purchased service </Text>
        <Text style={styles.textEdit} onPress={() =>navigate('EightPage')}> Whatching </Text>
        <Text style={styles.textEdit} onPress={() =>navigate('NinePage')}> Your favorite actors</Text>
        <Text style={styles.textEdit} onPress={() =>navigate('TenPage')}> Your favorite movies </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000000",

  },
  textlayout:{
    flex:1,
    backgroundColor:"black", 
    marginEnd: 20,
    alignItems:"flex-end"
  },
  textEdit:{
    fontWeight:"bold",
    fontStyle:"normal",
    color:"white",
    paddingVertical: 5,
    fontSize:15
  },
  textHead:{
    fontSize:25,
    color : "white",
    fontWeight:"bold"
  }
});
