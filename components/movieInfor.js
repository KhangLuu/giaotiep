import React, { Component } from 'react';
import {
  Animated,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  View,
  RefreshControl,
  ScrollView,
  TouchableOpacity

} from 'react-native';
import  Icon  from 'react-native-vector-icons/FontAwesome';
import Category from "./Category"

const HEADER_MAX_HEIGHT = 300;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 73;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const title = "Silent Voice"
const des ="A Silent Voice: The Movie (Japanese: 映画 聲の形, Hepburn: Eiga Koe no Katachi, also translated as The Shape of Voice: The Movie) is a 2016 Japanese animated teen drama film produced by Kyoto Animation, directed by Naoko Yamada and written by Reiko Yoshida, featuring character designs by Futoshi Nishiya and music by Kensuke Ushio.[3] It is based on the manga of the same name written and illustrated by Yoshitoki Ōima. The film premiered in Japan on September 17, 2016, and worldwide between February and June 2017. "


export default class movieInfo extends Component {
  static navigationOptions = {
    title:"",
    headerTransparent: {
      position: 'absolute',
      backgroundColor: 'transparent',
      zIndex: 100,
      top: 0,
      left: 0,
      right: 0
}
    }
  constructor(props) {
    super(props);

    this.state = {
      scrollY: new Animated.Value(
        // iOS has negative initial scroll value because content inset...
        Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
      ),
      refreshing: false,
    };
  }

  _renderScrollViewContent() {
    return (
      <View style={styles.scrollViewContent}>
          <Text style ={{color:"white", fontSize:30, paddingLeft:10, fontStyle:"italic", fontWeight:"bold", borderBottomColor:"white" ,borderBottomWidth:0.5,paddingBottom:3}}>Silent Voice</Text>
          <View style={{flexDirection:"row",}}>
            <Icon name="thumbs-up" style={styles.icon} size={20}></Icon> 
            <Text style ={{color:"white", paddingHorizontal:10}}>2019</ Text>
            <Text style ={{color:"white"  ,paddingHorizontal:10}}>16+</Text>
            <Text style ={{color:"white"  ,paddingHorizontal:10}} >HD</Text>
            <Icon name="picture-o" style={styles.icon} size ={20} ></Icon>
            </View>
            <View style={{ padding:10}}>
              <View style={{flexDirection:"row"}}>
            <Text style ={{color:"white", fontSize: 30 , paddingTop:10}}>WATCH IT NOW </Text>
            <Icon name="play-circle" size={40} style={{color:"#fff", paddingTop:10,paddingLeft:10}}></Icon>
            </View>
            
            <Text style ={{color:"white", fontSize:20} }>Describle :</Text>
            <View style={{ borderRadius: 4,borderWidth: 0.5,borderColor: '#d6d7da',}}>
            <Text style ={{color:"white",margin:5}}>{des}</Text>
            </View>
            </View>
            <View style={{flexDirection:"row" ,padding:20}}>
            <Icon name="bookmark-o" title="Add list" style={{color:"white",paddingHorizontal:20}} size={30}></Icon>
            <Icon name="star-o" title="Add list" style={{color:"white",paddingLeft:35}} size={30}></Icon>
            <Icon name="share-alt" title="Add list" style={{color:"white",paddingLeft:44}} size={30}></Icon>
            </View>
            <View style={{flexDirection:"row" ,paddingTop:4}}>
            <Text style ={{color:"white",paddingHorizontal:20}}>BookMark</Text>
            <Text style ={{color:"white",paddingHorizontal:15}}>Rate</Text>
            <Text style ={{color:"white",paddingHorizontal:20}}>Share</Text>
            </View>
            <View style={{ height: 200, marginTop: 0.5 ,padding:10 }}>
            <Text style={{color:"white" , textAlign:"center",backgroundColor:"#1f1f1f", fontSize:20,paddingBottom:10}}>Related movies</Text>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                <Category imageUri={require('../assets/(6)(6).jpg')}
                                    name="movie2"
                                />
                                <Category imageUri={require('../assets/(7)(7).jpg')}
                                    name="movie3"
                                />
                                <Category imageUri={require('../assets/(8)(8).jpg')}
                                    name="movie4"
                                />
                                <Category imageUri={require('../assets/(9)(9).jpg')}
                                    name="movie5"
                                />
                                <Category imageUri={require('../assets/(10)(10).jpg')}
                                    name="movie6"
                                />
                            </ScrollView>                            
                        </View>

        </View>
    );
  }

  render() {
    // Because of content inset the scroll value will be negative on iOS so bring
    // it back to 0.
    const scrollY = Animated.add(
      this.state.scrollY,
      Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
    );
    const headerTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -HEADER_SCROLL_DISTANCE],
      extrapolate: 'clamp',
    });

    const imageOpacity = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0],
      extrapolate: 'clamp',
    });
    const imageTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 100],
      extrapolate: 'clamp',
    });

    const titleScale = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0.8],
      extrapolate: 'clamp',
    });
    const titleTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 0, -8],
      extrapolate: 'clamp',
    });

    return (
      <View style={styles.fill}>
        <StatusBar
          translucent
          barStyle="light-content"
          backgroundColor="rgba(0, 0, 0, 0.251)"
        />
        <Animated.ScrollView
          style={styles.fill}
          scrollEventThrottle={1}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
            { useNativeDriver: true },
          )}
        >
          {this._renderScrollViewContent()}
        </Animated.ScrollView>
        <Animated.View
          pointerEvents="none"
          style={[
            styles.header,
            { transform: [{ translateY: headerTranslate }] },
          ]}
        >
          <Animated.Image
            style={[
              styles.backgroundImage,
              {
                opacity: imageOpacity,
                transform: [{ translateY: imageTranslate }],
              },
            ]}
            source={require('../assets/(6)(6).jpg')}
          />
        </Animated.View>
        <Animated.View
          style={[
            styles.bar,
            {
              transform: [
                { scale: titleScale },
                { translateY: titleTranslate },
              ],
            },
          ]}
        >
  <Text style={styles.title}>{title}</Text>
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  fill: {
    flex: 1,
    backgroundColor:"#000"
  },
  content: {
    flex: 1,
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#000',
    overflow: 'hidden',
    height: HEADER_MAX_HEIGHT,
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },
  bar: {
    backgroundColor: 'transparent',
    marginTop: Platform.OS === 'ios' ? 28 : 38,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  title: {
    color: 'white',
    fontSize: 20,
    fontStyle:"italic",
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
     textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10
  },
  scrollViewContent: {
   
    paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
  },
  row: {
    height: 40,
    margin: 16,
    backgroundColor: '#D3D3D3',
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon :{
    color:"#fff",
    paddingHorizontal:10
  }
});