import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TextInput ,TouchableOpacity} from 'react-native';
import { FlatGrid } from 'react-native-super-grid';
import Search from 'react-native-search-box';
import Icon from "react-native-vector-icons/FontAwesome"


export default class movieRE extends Component {
      updateSearch = search => {
        this.setState({ search });
      };
      static navigationOptions = {
        title: 'Movie Result' ,
        //Sets Header text of Status Bar
        headerStyle: {
          backgroundColor: '#000',
          //Sets Header color
        },
        headerTintColor: '#fff',
        //Sets Header text color
        headerTitleStyle: {
          fontWeight: 'bold',
          flex:1,
          textAlign:'left'
    
          //Sets Header text style
        },
      };
  render() {
      const {navigate} = this.props.navigation
    const items = [
      { name: 'Monster', code: './assets/1.jpg' }, { name: 'Monster', code: '../assets/1.jpg' },
      { name: 'Monster', code: './assets/1.jpg' }, { name: 'Monster', code: '../assets/1.jpg' },
      { name: 'Monster', code: './assets/1.jpg' }, { name: 'Monster', code: '../assets/1.jpg' },
      { name: 'Monster', code: './assets/1.jpg' }, { name: 'Monster', code: '../assets/1.jpg' },
      { name: 'Monster', code: './assets/1.jpg' }, { name: 'Monster', code: '../assets/1.jpg' },
      { name: 'Monster', code: '../assets/1.jpg' }, { name: 'Monster', code: '../assets/1.jpg' },
    ];
    

    return (
     
     <View style={styles.container}>
        <View>
      <Search
      backgroundColor="black"
      ></Search>
        </View>
      <FlatGrid
        itemDimension={100}
        items={items}
        style={styles.gridView}                                                                                                                                                                                                                         
        //  staticDimension={360}
        // fixed
        // spacing={20}
        renderItem={({ item, index }) => (
           
            <View style={[styles.itemContainer, { backgroundColor: "black" }]}>
            
            <Image source={require("../assets/(5).jpg")} style={{ flex: 1,width: null,height: null, resizeMode: 'cover'}}></Image>
            
            <TouchableOpacity onPress={()=>{navigate("movieInfor")}}>
            <Text style={styles.itemName}>{item.name}</Text>
            </TouchableOpacity>
          </View>
          
          
        )}
      /></View>
    );
  }
}

const styles = StyleSheet.create({
    container :{
        flex:1,
        backgroundColor:"black"
    },
  gridView: {
    marginTop: 20,
    flex: 1,
    backgroundColor:"black"},
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    textAlign:"center",
    color: '#ffffff',
    fontWeight: '600',
  },
  text: {
      flex:1,
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
    borderWidth:0.5,
    borderRadius:6,
    borderColor:"gray",
    paddingTop: 10,
    backgroundColor:"gray"
  },
  search:{
      flexDirection:"row",
      paddingTop:30
  }
});


