/**
 * @format
 */

import {AppRegistry} from 'react-native';
import profileheadermager from './profileheadermager';
import profile from './components/profile'
import noti from "./components/noti"
import watching from "./components/watching"
import actor from"./components/actor";
import movie from "./components/movie";
import transHis from "./components/transHistory"
import purcheSer from "./components/PurcheSer"
import payment from "./components/payment"
import home from "./components/homepage"
import pageManager from "./pageManager"
import movieInfor from "./components/movieInfor"
import actorInfor from "./components/actorinfo"
import movieRE from "./components/movieResult"
import actorRE from "./components/actorResult"

import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => pageManager);
