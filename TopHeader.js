
import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';
import home from "./components/homepage"
import allActor from "./components/allactor"
import allMovie from "./components/allmovie"
import movieInfor from "./components/movieInfor"
import actorInfor from "./components/actorinfo"
import noti from "./components/noti"
const topHeader = createStackNavigator({
    Homepage:{
        screen:home,
    },
    noti:{
        screen:noti
    },
    movieInfor:{
        screen:movieInfor
    },
    actorInfor:{
        screen:actorInfor
    }
 
},
{
    initialRouteName: 'Homepage',
}); 
const AppContainer = createAppContainer(topHeader) 
export default AppContainer

 