
import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';
import home from "./components/homepage"
import allActor from "./components/allactor"
import actorRE from "./components/actorResult"
import actorInfor from "./components/actorinfo"
import noti from "./components/noti"
const Actor = createStackNavigator({
    Actor:{
        screen: allActor,
    },
    RE:{
        screen:actorRE,
    }, 
    actorInfor:{
        screen:actorInfor
    }
},
{
    initialRouteName: 'Actor',
}); 
const AppContainer = createAppContainer(Actor) 
export default AppContainer

 