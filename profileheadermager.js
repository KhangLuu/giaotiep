
//This is an example code for Navigator// 
import React, { Component } from 'react';
//import react in our code. 
 
//For react-navigation 3.0+
//import { createAppContainer, createStackNavigator } from 'react-navigation';
//For react-navigation 4.0+
import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';
 
import profile from './components/profile'
import noti from "./components/noti"
import watching from "./components/watching"
import actor from"./components/actor";
import movie from "./components/movie";
import transHis from "./components/transHistory"
import purcheSer from "./components/PurcheSer"
import payment from "./components/payment"
import info from "./components/info"
import device from "./components/device"
import home from "./components/homepage"
import movieInfor from "./components/movieInfor"
//import all the screens we are going to switch 

const App = createStackNavigator({
    FirstPage: { screen: profile }, 
    SecondPage: { screen: noti }, 
    ThreePage: { screen: info}, 
    FourPage: { screen:device }, 
    FivePage: { screen: payment }, 
    SixPage: { screen: transHis }, 
    SevenPage: { screen: purcheSer }, 
    EightPage: { screen: watching }, 
    NinePage: { screen: actor }, 
    TenPage: { screen: movie }, 
    ElevenPage:{screen: home },
    TwePage : {screen: movieInfor}

  },
  {
    initialRouteName: 'FirstPage',
  }
);
const AppContainer = createAppContainer(App) 
export default AppContainer;