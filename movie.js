
import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';
import allMovie from "./components/allmovie"
import movieRE from "./components/movieResult"
import movieInfor from "./components/movieInfor"
const Movie = createStackNavigator({
    Movie:{
        screen: allMovie,
        navigationOptions:{
            title: "ALL Movie"
        }
    },
    RE:{
        screen:movieRE,
    }, 
    
    movieInfor:{
        screen:movieInfor
    }
},
{
    initialRouteName: 'Movie',
}); 
const AppContainer = createAppContainer(Movie) 
export default AppContainer

 